<?php

//xss mitigation function
function xssafe($data) {
    return htmlspecialchars($data, ENT_QUOTES | ENT_HTML401, 'UTF-8');
}

?>

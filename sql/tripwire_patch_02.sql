--
-- Table structure for table `_history_esi`
--

CREATE TABLE IF NOT EXISTS `_history_esi` (
  `userID` int(11) NOT NULL,
  `characterID` int(10) unsigned NOT NULL,
  `characterName` varchar(100) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for table `_history_esi`
--

ALTER TABLE `_history_esi`
  ADD PRIMARY KEY (`userID`,`characterID`);

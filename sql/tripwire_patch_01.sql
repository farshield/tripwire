--
-- Table structure for table `_history_comments`
--

CREATE TABLE IF NOT EXISTS `_history_comments` (
  `historyID` int(11) NOT NULL AUTO_INCREMENT,
  `id` int(11) NOT NULL,
  `systemID` int(8) unsigned NOT NULL,
  `comment` text NOT NULL,
  `created` datetime NOT NULL,
  `createdBy` int(10) NOT NULL,
  `modified` datetime NOT NULL,
  `modifiedBy` int(10) NOT NULL,
  `maskID` decimal(12,2) NOT NULL,
  `requested` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `requestedBy` int(10) NOT NULL,
  `mode` enum('save','delete','sticky') NOT NULL,
  PRIMARY KEY (`historyID`)
) ENGINE=InnoDB AUTO_INCREMENT=1337 DEFAULT CHARSET=utf8;

--
-- Table structure for table `_history_masks`
--

CREATE TABLE IF NOT EXISTS `_history_masks` (
  `historyID` int(11) NOT NULL AUTO_INCREMENT,
  `maskID` decimal(12,1) unsigned NOT NULL,
  `name` varchar(100),
  `ownerID` int(10) unsigned,
  `ownerType` smallint(5) unsigned,
  `adds` varchar(100),
  `deletes` varchar(100),
  `requested` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `requestedBy` int(11) NOT NULL,
  `mode` enum('create','delete','save') NOT NULL,
  PRIMARY KEY (`historyID`)
) AUTO_INCREMENT=2337 DEFAULT CHARSET=utf8;

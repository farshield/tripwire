<?php

// EVE SDE table name
$eve_dump = 'eve_sde';

// Set php timezone to EVE time
date_default_timezone_set('UTC');

// EVE API userAgent
$userAgent = 'Tripwire Server - adminEmail@example.com';

// EVE SSO info
$evessoClient = 'clientID';
$evessoSecret = 'secret';
$evessoRedirect = 'http://localhost/login.php?mode=sso';

// Login management
$apiLoginEnabled = True;

try {
    $mysql = new PDO(
        'mysql:host=localhost;dbname=tripwire_database;charset=utf8',
        'username',
        'password',
        Array(
            PDO::ATTR_PERSISTENT     => true,
            PDO::MYSQL_ATTR_LOCAL_INFILE => true
        )
    );
    // Set MySQL timezone to EVE time
    $mysql->exec("SET time_zone='+00:00';");
} catch (PDOException $error) {
    error_log($error->getMessage());
}

?>

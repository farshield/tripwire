<?php

if (!session_id()) session_start();
session_write_close();

// Check for login permission - else kick
if(!isset($_SESSION['userID'])){
	exit();
}

require('db.inc.php');
require('lib.inc.php');

header('Content-Type: application/json');

$startTime = microtime(true);
$mask = $_SESSION['mask'];
$output = null;

$maskCheck = explode('.', $mask);
if ($maskCheck[1] == 2) {
	$query = 'SELECT characterID, characterName, corporationName, sigCount, systemsVisited, systemsViewed FROM userStats s INNER JOIN characters c ON s.userID = c.userID WHERE corporationID = :corporationID';
	$stmt = $mysql->prepare($query);
	$stmt->bindValue(':corporationID', $maskCheck[0]);
} else {
	$query = 'SELECT characterID, characterName, corporationName, sigCount, systemsVisited, systemsViewed FROM userStats s INNER JOIN characters c ON s.userID = c.userID WHERE (corporationID IN (SELECT eveID FROM groups WHERE eveType = 2 AND joined = 1 AND maskID = :mask UNION SELECT ownerID FROM masks WHERE ownerType = 2 AND maskID = :mask) OR characterID IN (SELECT eveID FROM groups WHERE eveType = 1373 AND joined = 1 AND maskID = :mask UNION SELECT ownerID FROM masks WHERE ownerType = 1373 AND maskID = :mask))';
	$stmt = $mysql->prepare($query);
	$stmt->bindValue(':mask', $mask);
}

$stmt->execute();
$output['results'] = $stmt->fetchAll(PDO::FETCH_CLASS);
$output['proccessTime'] = sprintf('%.4f', microtime(true) - $startTime);

echo json_encode($output);
?>
